# Notes

I think i might have over-engineered this solution especially around discount package.
ServiceLocator although naive fits better than hardcoded implementations
Java was updated to 11 to allow 'var' and 'private methods' in interfaces - not to much value added but code looks better ;)
Tests - average coverage around 'model', good around discount/calculator packages - due to the BasketTest that exercise a lot of code. Some smaller 'units' missing.