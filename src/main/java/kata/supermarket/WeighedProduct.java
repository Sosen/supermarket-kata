package kata.supermarket;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

import static java.util.Collections.singleton;

public class WeighedProduct extends AbstractProduct {

    private final BigDecimal pricePerKilo;

    public WeighedProduct(final String name, final ProductCategory category, final BigDecimal pricePerKilo) {
        this(name, singleton(category), pricePerKilo);
    }

    public WeighedProduct(final String name, final Set<ProductCategory> categories, final BigDecimal pricePerKilo) {
        super(name, categories);
        this.pricePerKilo = pricePerKilo;
    }

    BigDecimal pricePerKilo() {
        return pricePerKilo;
    }

    public Item weighing(final BigDecimal kilos) {
        return new ItemByWeight(this, kilos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeighedProduct)) return false;
        if (!super.equals(o)) return false;
        WeighedProduct that = (WeighedProduct) o;
        return Objects.equals(pricePerKilo, that.pricePerKilo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pricePerKilo);
    }
}
