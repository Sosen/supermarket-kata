package kata.supermarket.discount;

import kata.supermarket.ProductCategory;

final public class BuyXForPriceOfYPerCategoryDiscounter extends AbstractBuyXForPriceOfYDiscounter {

    private static final String DESCRIPTION = "Buy %d of category %s for price of %d";

    private final ProductCategory productCategory;


    BuyXForPriceOfYPerCategoryDiscounter(ProductCategory productCategory, int requiredItemsCount, int payForItemsCount) {
        this(productCategory, requiredItemsCount, payForItemsCount, DEFAULT_PRIORITY);
    }

    BuyXForPriceOfYPerCategoryDiscounter(ProductCategory productCategory, int requiredItemsCount, int payForItemsCount, int priority) {
        super(requiredItemsCount, payForItemsCount, item -> item.product().hasCategory(productCategory), priority);
        this.productCategory = productCategory;
    }

    @Override
    public String description() {
        return String.format(DESCRIPTION, getRequiredItemsCount(), productCategory, getPayForItemsCount());
    }
}
