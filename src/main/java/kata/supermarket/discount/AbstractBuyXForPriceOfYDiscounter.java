package kata.supermarket.discount;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

abstract class AbstractBuyXForPriceOfYDiscounter extends AbstractBuyXDiscounter {

    private final int payForItemsCount;


    AbstractBuyXForPriceOfYDiscounter(int requiredItemsCount, int payForItemsCount, Predicate<Item> matchingItemCriteria, int priority) {
        super(requiredItemsCount, matchingItemCriteria, getAmountCalculation(payForItemsCount), priority);
        this.payForItemsCount = payForItemsCount;
    }

    private static Function<List<Item>, BigDecimal> getAmountCalculation(int payForItemsCount) {
        return matchingItems -> {

            var totalAmount = matchingItems.stream()
                    .map(Item::price).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
            var payedForAmount = matchingItems.stream()
                    .limit(payForItemsCount)
                    .map(Item::price).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

            return totalAmount.subtract(payedForAmount);
        };
    }

    public int getPayForItemsCount() {
        return payForItemsCount;
    }
}
