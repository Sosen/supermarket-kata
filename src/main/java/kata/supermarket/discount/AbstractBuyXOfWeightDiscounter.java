package kata.supermarket.discount;

import kata.supermarket.Item;
import kata.supermarket.ItemByWeight;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

abstract class AbstractBuyXOfWeightDiscounter extends AbstractDiscounter {

    private final BigDecimal requiredItemsWeight;
    private final Predicate<ItemByWeight> matchingItemCriteria;
    private final Function<List<ItemByWeight>, BigDecimal> discountCalculation;


    AbstractBuyXOfWeightDiscounter(BigDecimal requiredItemsWeight, Predicate<ItemByWeight> matchingItemCriteria, Function<List<ItemByWeight>, BigDecimal> discountCalculation, int priority) {
        super(priority);
        this.requiredItemsWeight = requiredItemsWeight;
        this.matchingItemCriteria = matchingItemCriteria;
        this.discountCalculation = discountCalculation;
    }

    @Override
    public DiscountResult applyDiscount(List<Item> items) {
        var matchingItems = findMatchingItems(items);

        var totalWeight = matchingItems.stream()
                .map(ItemByWeight::weightInKilos)
                .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

        if (totalWeight.compareTo(requiredItemsWeight) >= 0) {
            return new DiscountResult(discountCalculation.apply(matchingItems), description(), matchingItems);
        }

        return DiscountResult.NO_DISCOUNT;
    }

    protected List<ItemByWeight> findMatchingItems(List<Item> items) {
        return items.stream()
                .filter(item -> ItemByWeight.class.isAssignableFrom(item.getClass()))
                .map(ItemByWeight.class::cast)
                .filter(matchingItemCriteria)
                .sorted(Comparator.comparing(Item::price).reversed())
                .collect(toList());
    }

    public BigDecimal getRequiredItemsWeight() {
        return requiredItemsWeight;
    }
}

