package kata.supermarket.discount;

import kata.supermarket.Item;

import java.util.List;

public interface Discounter {

    int LOWEST_PRIORITY = Integer.MIN_VALUE;
    int DEFAULT_PRIORITY = 0;
    int HIGHETS_PRIORITY = Integer.MAX_VALUE;

    DiscountResult applyDiscount(List<Item> items);

    String description();

    default int priority() {
        return DEFAULT_PRIORITY;
    }
}
