package kata.supermarket.discount;

import java.util.List;

public interface DiscountProvider {

    List<Discounter> provide();
}
