package kata.supermarket.discount;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.util.List;

final public class BuyXGet1FreePerProductDiscounter extends AbstractDiscounter {

    private static final String DESCRIPTION = "Buy %d of product %s get 1 free";
    private static final int FREE_ITEM_COUNT = 1;

    private final String productName;
    private final int requiredItemsCount;
    private final Discounter delegate;

    BuyXGet1FreePerProductDiscounter(String productName, int requiredItemsCount) {
        this(productName, requiredItemsCount, DEFAULT_PRIORITY);
    }

    BuyXGet1FreePerProductDiscounter(String productName, int requiredItemsCount, int priority) {
        super(priority);
        this.productName = productName;
        this.requiredItemsCount = requiredItemsCount;
        this.delegate = new BuyXForPriceOfYPerProductDiscounter(productName, requiredItemsCount + FREE_ITEM_COUNT, requiredItemsCount);
    }

    @Override
    public DiscountResult applyDiscount(List<Item> items) {
        DiscountResult result = delegate.applyDiscount(items);

        if (result.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            return new DiscountResult(result.getAmount(), description(), result.getDiscountedItems());
        }

        return result;
    }

    @Override
    public String description() {
        return String.format(DESCRIPTION, requiredItemsCount, productName);
    }

    @Override
    public int priority() {
        return Discounter.HIGHETS_PRIORITY;
    }
}
