package kata.supermarket.discount;

import kata.supermarket.Item;
import kata.supermarket.ServiceLocator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Iterate over all available discount based on their priority
 */
public class PrioritisedDiscountChain implements DiscountChain {

    @Override
    public BigDecimal applyDiscount(List<Item> items) {
        List<Discounter> discounters = ServiceLocator.getInstance(DiscountProvider.class).provide().stream().sorted(Comparator.comparing(Discounter::priority).reversed().thenComparing(Discounter::description)).collect(Collectors.toList());
        List<Item> modifyableItems = new ArrayList<>(items);
        return discounters.stream().map(discounter -> processDiscounts(discounter, modifyableItems)).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }

    private BigDecimal processDiscounts(Discounter discounter, List<Item> items) {
        DiscountResult result = discounter.applyDiscount(items);
        BigDecimal amount = result.getAmount();
        if (amount.compareTo(BigDecimal.ZERO) > 0) {
            items.removeAll(result.getDiscountedItems());
            amount = amount.add(processDiscounts(discounter, items));
        }
        return amount;
    }

}
