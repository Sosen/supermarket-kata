package kata.supermarket.discount;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

final public class BuyXForFixedPricePerProductDiscounter extends AbstractBuyXDiscounter {

    private static final String DESCRIPTION = "Buy %d of product %s for %.2f";

    private final String productName;
    private final int requiredItemsCount;
    private final BigDecimal fixedPrice;

    BuyXForFixedPricePerProductDiscounter(String productName, int requiredItemsCount, BigDecimal fixedPrice) {
        this(productName, requiredItemsCount, fixedPrice, DEFAULT_PRIORITY);
    }

    public BuyXForFixedPricePerProductDiscounter(String productName, int requiredItemsCount, BigDecimal fixedPrice, int priority) {
        super(requiredItemsCount, item -> item.product().name().equals(productName), getAmountCalculation(fixedPrice), priority);
        this.productName = productName;
        this.requiredItemsCount = requiredItemsCount;
        this.fixedPrice = fixedPrice;
    }

    private static Function<List<Item>, BigDecimal> getAmountCalculation(BigDecimal fixedPrice) {
        return matchingItems -> matchingItems.stream()
                .map(Item::price).reduce(BigDecimal::add).orElse(BigDecimal.ZERO)
                .subtract(fixedPrice).abs();
    }

    @Override
    public String description() {
        return String.format(DESCRIPTION, requiredItemsCount, productName, fixedPrice);
    }
}
