package kata.supermarket.discount;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

abstract class AbstractBuyXDiscounter extends AbstractDiscounter {

    private final int requiredItemsCount;
    private final Predicate<Item> matchingItemCriteria;
    private final Function<List<Item>, BigDecimal> discountCalculation;

    AbstractBuyXDiscounter(int requiredItemsCount, Predicate<Item> matchingItemCriteria, Function<List<Item>, BigDecimal> discountCalculation, int priority) {
        super(priority);
        this.requiredItemsCount = requiredItemsCount;
        this.matchingItemCriteria = matchingItemCriteria;
        this.discountCalculation = discountCalculation;
    }

    @Override
    public DiscountResult applyDiscount(List<Item> items) {
        var matchingItems = findMatchingItems(items);

        if (matchingItems.size() == requiredItemsCount) {
            return new DiscountResult(discountCalculation.apply(matchingItems), description(), matchingItems);
        }
        return DiscountResult.NO_DISCOUNT;
    }

    protected List<Item> findMatchingItems(List<Item> items) {
        return items.stream()
                .filter(matchingItemCriteria)
                .sorted(Comparator.comparing(Item::price).reversed())
                .limit(requiredItemsCount)
                .collect(toList());
    }

    public int getRequiredItemsCount() {
        return requiredItemsCount;
    }

}
