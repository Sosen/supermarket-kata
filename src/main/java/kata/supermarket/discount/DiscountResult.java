package kata.supermarket.discount;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;

public class DiscountResult {

    static final DiscountResult NO_DISCOUNT = new DiscountResult(BigDecimal.ZERO, "No discount", emptyList());

    private final BigDecimal amount;

    private final String description;

    private final List<? extends Item> discountedItems;


    public DiscountResult(BigDecimal amount, String description, List<? extends Item> discountedItems) {
        Objects.requireNonNull(amount, "Amount is required");
        Objects.requireNonNull(discountedItems, "Discounted item shoun't not be null");
        this.amount = amount.setScale(2, RoundingMode.HALF_UP);
        this.description = description;
        this.discountedItems = discountedItems;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public List<? extends Item> getDiscountedItems() {
        return discountedItems;
    }
}
