package kata.supermarket.discount;

import kata.supermarket.Item;
import kata.supermarket.ItemByWeight;
import kata.supermarket.ProductCategory;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

public class BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounter extends AbstractBuyXOfWeightDiscounter {

    private static final String DESCRIPTION = "Buy %.2f kilos of category %s with discount of %d percent";
    private static final int HUNDRED = 100;

    private ProductCategory productCategory;

    private int discountPercentage;

    BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounter(ProductCategory productCategory, BigDecimal requiredItemsWeight, int discountPercentage) {
        this(productCategory, requiredItemsWeight, discountPercentage, DEFAULT_PRIORITY);
    }


    BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounter(ProductCategory productCategory, BigDecimal requiredItemsWeight, int discountPercentage, int priority) {
        super(requiredItemsWeight, item -> item.product().hasCategory(productCategory), getAmountCalculation(discountPercentage), priority);
        this.discountPercentage = discountPercentage;
        this.productCategory = productCategory;
    }


    private static Function<List<ItemByWeight>, BigDecimal> getAmountCalculation(int discountPercentage) {
        return matchingItems -> matchingItems.stream()
                .map(Item::price).reduce(BigDecimal::add).orElse(BigDecimal.ZERO)
                .multiply(BigDecimal.valueOf(discountPercentage).divide(BigDecimal.valueOf(HUNDRED)));
    }

    @Override
    public String description() {
        return String.format(DESCRIPTION, this.getRequiredItemsWeight(), productCategory, discountPercentage);
    }
}
