package kata.supermarket.discount;

abstract class AbstractDiscounter implements Discounter {

    private final int priority;

    public AbstractDiscounter(int priority) {
        this.priority = priority;
    }

    @Override
    public int priority() {
        return priority;
    }
}
