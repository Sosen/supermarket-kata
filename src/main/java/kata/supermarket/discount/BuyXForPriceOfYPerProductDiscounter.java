package kata.supermarket.discount;

final public class BuyXForPriceOfYPerProductDiscounter extends AbstractBuyXForPriceOfYDiscounter {

    private static final String DESCRIPTION = "Buy %d of product %s for price of %d";

    private final String productName;


    BuyXForPriceOfYPerProductDiscounter(String productName, int reqiredItemsCount, int payForItemsCount) {
        this(productName, reqiredItemsCount, payForItemsCount, DEFAULT_PRIORITY);
    }

    BuyXForPriceOfYPerProductDiscounter(String productName, int reqiredItemsCount, int payForItemsCount, int priority) {
        super(reqiredItemsCount, payForItemsCount, item -> item.product().name().equals(productName), priority);
        this.productName = productName;
    }

    @Override
    public String description() {
        return String.format(DESCRIPTION, getRequiredItemsCount(), productName, getPayForItemsCount());
    }
}
