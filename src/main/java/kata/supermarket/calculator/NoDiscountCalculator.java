package kata.supermarket.calculator;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.util.List;

public class NoDiscountCalculator implements TotalCalculator {

    @Override
    public BigDecimal discounts(List<Item> items) {
        return BigDecimal.ZERO;
    }
}
