package kata.supermarket.calculator;

import kata.supermarket.Item;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public interface TotalCalculator {

    private BigDecimal subtotal(List<Item> items) {
        return items.stream().map(Item::price)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO)
                .setScale(2, RoundingMode.HALF_UP);
    }


    BigDecimal discounts(List<Item> items);

    default BigDecimal calculate(List<Item> items) {
        return subtotal(items).subtract(discounts(items));
    }
}
