package kata.supermarket.calculator;

import kata.supermarket.Item;
import kata.supermarket.ServiceLocator;
import kata.supermarket.discount.DiscountChain;

import java.math.BigDecimal;
import java.util.List;

public class DiscountedCalculator implements TotalCalculator {

    @Override
    public BigDecimal discounts(List<Item> items) {
        return ServiceLocator.getInstance(DiscountChain.class).applyDiscount(items);
    }
}
