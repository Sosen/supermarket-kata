package kata.supermarket;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

abstract class AbstractProduct implements Product {

    private final String name;

    private final Set<ProductCategory> categories = new HashSet<>();

    public AbstractProduct(String name, Set<ProductCategory> categories) {
        Objects.requireNonNull(categories, "Categories are required.");
        this.name = name;
        this.categories.addAll(categories);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public boolean hasCategory(ProductCategory category) {
        return categories.contains(category);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractProduct)) return false;
        AbstractProduct that = (AbstractProduct) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(categories, that.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, categories);
    }
}
