package kata.supermarket;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class ItemByWeight extends AbstractItem<WeighedProduct> {

    private final WeighedProduct product;
    private final BigDecimal weightInKilos;

    ItemByWeight(final WeighedProduct product, final BigDecimal weightInKilos) {
        super(product);
        this.product = product;
        this.weightInKilos = weightInKilos;
    }

    public BigDecimal price() {
        return product.pricePerKilo().multiply(weightInKilos).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal weightInKilos() {
        return weightInKilos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemByWeight)) return false;
        if (!super.equals(o)) return false;
        ItemByWeight that = (ItemByWeight) o;
        return Objects.equals(product, that.product) &&
                Objects.equals(weightInKilos, that.weightInKilos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), product, weightInKilos);
    }
}
