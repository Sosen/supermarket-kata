package kata.supermarket;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

public class ServiceLocator {

    private static final ConcurrentHashMap<Class, Supplier> INSTANCE_SUPPLIERS = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Class, Object> INSTANCES = new ConcurrentHashMap<>();

    /**
     * Get instance
     *
     * @param clazz
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T getInstance(Class<T> clazz) {
        return (T) INSTANCES.computeIfAbsent(clazz,
                c -> INSTANCE_SUPPLIERS.getOrDefault(clazz,
                        () -> {
                            throw new IllegalStateException(String.format("No instance supplier for class: %s", clazz.getName()));
                        })
                        .get()
        );
    }

    /**
     * Register supplier for given interface. Supplier will be use to create instance when needed
     *
     * @param clazz
     * @param supplier
     */
    public static void registerInstance(Class clazz, Supplier supplier) {
        if (!clazz.isInterface()) {
            throw new IllegalStateException(String.format("Provided class %s is not interface, only interfaces can be registered", clazz.getName()));
        }
        INSTANCE_SUPPLIERS.putIfAbsent(clazz, supplier);
    }
}
