package kata.supermarket;

public interface Product {

    String name();

    boolean hasCategory(ProductCategory category);
}
