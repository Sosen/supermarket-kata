package kata.supermarket;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

public interface Item {

    BigDecimal price();

    OffsetDateTime insertTime();

    Product product();
}
