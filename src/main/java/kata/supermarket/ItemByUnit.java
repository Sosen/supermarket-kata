package kata.supermarket;

import java.math.BigDecimal;
import java.util.Objects;

public class ItemByUnit extends AbstractItem<UnitProduct> {

    private final UnitProduct product;

    ItemByUnit(final UnitProduct product) {
        super(product);
        this.product = product;
    }

    public BigDecimal price() {
        return product.pricePerUnit();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ItemByUnit)) return false;
        if (!super.equals(o)) return false;
        ItemByUnit that = (ItemByUnit) o;
        return Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), product);
    }
}
