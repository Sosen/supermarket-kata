package kata.supermarket;

/**
 * In real world this would not be a hard-coded enum but rather tree-like model of categories and parent categories.
 */
public enum ProductCategory {

    DAIRY, VEGETABLES, SWEETS, ALCOHOL
}
