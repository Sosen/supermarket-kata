package kata.supermarket;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

import static java.util.Collections.singleton;

public class UnitProduct extends AbstractProduct {

    private final BigDecimal pricePerUnit;

    public UnitProduct(String name, ProductCategory category, final BigDecimal pricePerUnit) {
        this(name, singleton(category), pricePerUnit);
    }

    public UnitProduct(String name, Set<ProductCategory> categories, final BigDecimal pricePerUnit) {
        super(name, categories);
        this.pricePerUnit = pricePerUnit;
    }

    BigDecimal pricePerUnit() {
        return pricePerUnit;
    }

    public Item oneOf() {
        return new ItemByUnit(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UnitProduct)) return false;
        if (!super.equals(o)) return false;
        UnitProduct that = (UnitProduct) o;
        return Objects.equals(pricePerUnit, that.pricePerUnit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pricePerUnit);
    }
}
