package kata.supermarket;

import java.time.OffsetDateTime;
import java.util.Objects;

abstract class AbstractItem<PRODUCT extends Product> implements Item {

    private final OffsetDateTime insertTime;
    private final PRODUCT product;

    protected AbstractItem(PRODUCT product) {
        this.insertTime = OffsetDateTime.now();
        this.product = product;
    }


    @Override
    public OffsetDateTime insertTime() {
        return insertTime;
    }

    @Override
    public PRODUCT product() {
        return product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractItem)) return false;
        AbstractItem that = (AbstractItem) o;
        return Objects.equals(insertTime, that.insertTime) &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(insertTime, product);
    }
}
