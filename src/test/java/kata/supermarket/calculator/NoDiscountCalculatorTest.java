package kata.supermarket.calculator;

import kata.supermarket.Item;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static kata.supermarket.TestCommons.aPintOfMilk;
import static kata.supermarket.TestCommons.assertAmount;

class NoDiscountCalculatorTest {

    @DisplayName("No discount is applied when items are...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void appliedDiscount(String description, BigDecimal expectedDiscount, List<Item> items) {
        NoDiscountCalculator calculator = new NoDiscountCalculator();
        BigDecimal result = calculator.discounts(items);
        assertAmount(expectedDiscount, result);
    }

    static Stream<Arguments> appliedDiscount() {
        return Stream.of(
                aEmptyItems(),
                aNullCollectionOfItems(),
                aCollectionItems()
        );
    }

    private static Arguments aEmptyItems() {
        return Arguments.of("a empty collection of items is passed", BigDecimal.ZERO, emptyList());
    }

    private static Arguments aNullCollectionOfItems() {
        return Arguments.of("a null collection of items is passed", BigDecimal.ZERO, null);
    }

    private static Arguments aCollectionItems() {
        return Arguments.of("a collection of items is passed", BigDecimal.ZERO, asList(aPintOfMilk(), aPintOfMilk()));
    }

}