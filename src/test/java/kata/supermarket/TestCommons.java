package kata.supermarket;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCommons {

    public static final String EXPECTED_NO_DISCOUNT_DESCRIPTION = "No discount";
    public static final BigDecimal EXPECTED_NO_DISCOUNT = BigDecimal.ZERO.setScale(2);

    public static Item aPintOfMilk() {
        return new UnitProduct("Milk", ProductCategory.DAIRY, new BigDecimal("0.49")).oneOf();
    }

    public static Item aPintOfKefir() {
        return new UnitProduct("Kefir", ProductCategory.DAIRY, new BigDecimal("0.80")).oneOf();
    }

    public static Item aPintOfBeer() {
        return new UnitProduct("Beer", ProductCategory.ALCOHOL, new BigDecimal("3.50")).oneOf();
    }

    public static Item aKiloOfAmericanSweets(BigDecimal weight) {
        return new WeighedProduct("American Sweets", ProductCategory.SWEETS, new BigDecimal("4.99")).weighing(weight);
    }

    public static Item aKiloOfTomatos(BigDecimal weight) {
        return new WeighedProduct("Tomato", ProductCategory.VEGETABLES, new BigDecimal("2.49")).weighing(weight);
    }

    public static boolean compareCollectionsWithoutOrder(Collection expected, Collection actual){
        return new HashSet<>(expected).equals(new HashSet(actual));
    }

    public static void assertAmount(BigDecimal expected, BigDecimal actual){
        assertEquals(expected.setScale(2), actual.setScale(2));
    }

}
