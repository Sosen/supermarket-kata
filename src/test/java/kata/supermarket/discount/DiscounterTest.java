package kata.supermarket.discount;

import kata.supermarket.Item;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiscounterTest {

    @Test
    void testDefaultPriority(){
        assertEquals(Discounter.DEFAULT_PRIORITY, new Discounter(){
            @Override
            public DiscountResult applyDiscount(List<Item> items) {
                throw new UnsupportedOperationException();
            }

            @Override
            public String description() {
                throw new UnsupportedOperationException();
            }
        }.priority());
    }
}