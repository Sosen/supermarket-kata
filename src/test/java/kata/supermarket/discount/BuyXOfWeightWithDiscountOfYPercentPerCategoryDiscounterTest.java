package kata.supermarket.discount;

import kata.supermarket.Item;
import kata.supermarket.ProductCategory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static kata.supermarket.TestCommons.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounterTest {

    private static final ProductCategory REQUIRED_CATEGORY = ProductCategory.SWEETS;
    private static final BigDecimal REQUIRED_ITEMS_WEIGHT = BigDecimal.ONE;
    private static final int DISCOUNT_OF_PERCENT = 50;
    private static final String EXPECTED_DISCOUNT_DESCRIPTION = "Buy 1.00 kilos of category SWEETS with discount of 50 percent";


    private BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounter discounter = new BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounter(REQUIRED_CATEGORY, REQUIRED_ITEMS_WEIGHT, DISCOUNT_OF_PERCENT );

    @DisplayName("Discounter applies discount when items are...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void appliedDiscount(String description, BigDecimal expectedDiscount, String expectedDescription, Collection<Item> discountedItems, List<Item> items) {
        DiscountResult result = discounter.applyDiscount(items);
        assertAmount(expectedDiscount, result.getAmount());
        assertEquals(expectedDescription, result.getDescription());
        assertTrue(compareCollectionsWithoutOrder(discountedItems, result.getDiscountedItems()));
    }

    static Stream<Arguments> appliedDiscount() {
        return Stream.of(
                aSingleExpectedBelowWeightItem(),
                aSingleExpectedEqualWeightItem(),
                aSingleExpectedAboveWeightItem(),
                aMultipleItemsEqualWeightSameProduct(),
                aMultipleItemsAboveWeightSameProduct(),
                aSingleUnexpectedEqualWeightItem(),
                aMultipleUnexpectedItems(),
                aMultipleUnexpectedItemsNotSupportingWeight()
        );
    }

    private static Arguments aSingleExpectedBelowWeightItem() {
        return Arguments.of("a single expected item below weight is not discounted", EXPECTED_NO_DISCOUNT, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), singletonList(aKiloOfAmericanSweets(BigDecimal.valueOf(0.4))));
    }

    private static Arguments aSingleExpectedEqualWeightItem() {
        List<Item> item = singletonList(aKiloOfAmericanSweets(BigDecimal.ONE));
        return Arguments.of("a single expected item equal weight is discounted", BigDecimal.valueOf(2.50), EXPECTED_DISCOUNT_DESCRIPTION, item, item);
    }

    private static Arguments aSingleExpectedAboveWeightItem() {
        List<Item> item = singletonList(aKiloOfAmericanSweets(BigDecimal.valueOf(1.4)));
        return Arguments.of("a single expected item above weight is discounted", BigDecimal.valueOf(3.50), EXPECTED_DISCOUNT_DESCRIPTION, item, item);
    }

    private static Arguments aMultipleItemsEqualWeightSameProduct() {
        List<Item> items = asList(aKiloOfAmericanSweets(BigDecimal.valueOf(0.4)), aKiloOfAmericanSweets(BigDecimal.valueOf(0.4)), aKiloOfAmericanSweets(BigDecimal.valueOf(0.2)));
        return Arguments.of("a multiple items (of the same product) equals weight is discounted", BigDecimal.valueOf(2.50), EXPECTED_DISCOUNT_DESCRIPTION, items, items);
    }

    private static Arguments aMultipleItemsAboveWeightSameProduct() {
        List<Item> items = asList(aKiloOfAmericanSweets(BigDecimal.valueOf(0.4)), aKiloOfAmericanSweets(BigDecimal.valueOf(0.4)), aKiloOfAmericanSweets(BigDecimal.valueOf(0.6)));
        return Arguments.of("a multiple items (of the same product) equals weight is discounted", BigDecimal.valueOf(3.50), EXPECTED_DISCOUNT_DESCRIPTION, items, items);
    }

    private static Arguments aSingleUnexpectedEqualWeightItem() {
        return Arguments.of("a single unexpected item equal weight is discounted", EXPECTED_NO_DISCOUNT, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), singletonList(aKiloOfTomatos(BigDecimal.ONE)));
    }

    private static Arguments aMultipleUnexpectedItems() {
        List<Item> items = asList(aKiloOfTomatos(BigDecimal.valueOf(0.4)), aKiloOfTomatos(BigDecimal.valueOf(0.4)), aKiloOfTomatos(BigDecimal.valueOf(0.2)));
        return Arguments.of("a multiple unexpected items equal weight is not discounted", EXPECTED_NO_DISCOUNT, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), items);
    }

    private static Arguments aMultipleUnexpectedItemsNotSupportingWeight() {
        List<Item> items = asList(aPintOfBeer(), aPintOfMilk(), aPintOfKefir());
        return Arguments.of("a multiple unexpected items equal weight is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), items);
    }

}