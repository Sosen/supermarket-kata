package kata.supermarket.discount;

import kata.supermarket.Item;
import kata.supermarket.ProductCategory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static kata.supermarket.TestCommons.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BuyXForPriceOfYPerCategoryDiscounterTest {

    private static final ProductCategory REQUIRED_CATEGORY = ProductCategory.DAIRY;
    private static final int REQUIRED_ITEMS_COUNT = 3;
    private static final int PAY_FOR_COUNT = REQUIRED_ITEMS_COUNT - 1;
    private static final String EXPECTED_DISCOUNT_DESCRIPTION = "Buy 3 of category DAIRY for price of 2";
    private static final BigDecimal EXPECTED_DISCOUNT = BigDecimal.valueOf(0.49);


    private BuyXForPriceOfYPerCategoryDiscounter discounter = new BuyXForPriceOfYPerCategoryDiscounter(REQUIRED_CATEGORY, REQUIRED_ITEMS_COUNT, PAY_FOR_COUNT );

    @DisplayName("Discounter applies discount when items are...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void appliedDiscount(String description, BigDecimal expectedDiscount, String expectedDescription, Collection<Item> discountedItems, List<Item> items) {
        DiscountResult result = discounter.applyDiscount(items);
        assertAmount(expectedDiscount, result.getAmount());
        assertEquals(expectedDescription, result.getDescription());
        assertTrue(compareCollectionsWithoutOrder(discountedItems, result.getDiscountedItems()));
    }

    static Stream<Arguments> appliedDiscount() {
        return Stream.of(
                aSingleExpectedItem(),
                aLesserNumberOfExpectedItems(),
                aExactNumberOfExpectedItemsTheSameProduct(),
                aExactNumberOfExpectedItemsMixedProduct(),
                aGreaterNumberOfExpectedItemsTheSameProduct(),
                aGreaterNumberOfExpectedItemsMixedProduct(),
                aExactNumberOfUnexpectedItems(),
                aExactNumberOfExpectedMixedWithUnexpectedItems()
        );
    }

    private static Arguments aSingleExpectedItem() {
        return Arguments.of("a single expected item is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), singletonList(aPintOfMilk()));
    }

    private static Arguments aLesserNumberOfExpectedItems() {
        return Arguments.of("a lesser number of expected items is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), asList(aPintOfMilk(), aPintOfMilk()));
    }

    private static Arguments aExactNumberOfExpectedItemsTheSameProduct() {
        List<Item> items = asList(aPintOfMilk(), aPintOfMilk(), aPintOfMilk());
        return Arguments.of("a exact number of expected items of the same product is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, items, items);
    }

    private static Arguments aExactNumberOfExpectedItemsMixedProduct() {
        List<Item> items = asList(aPintOfMilk(), aPintOfMilk(), aPintOfKefir());
        return Arguments.of("a exact number of expected items of mixed product is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, items, items);
    }

    private static Arguments aGreaterNumberOfExpectedItemsTheSameProduct() {
        List<Item> expectedItems = asList(aPintOfMilk(), aPintOfMilk(), aPintOfMilk());
        List<Item> items = new ArrayList<>(expectedItems);
        items.add(aPintOfMilk());
        return Arguments.of("a greater number of expected items of the same product is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, expectedItems, items);
    }

    private static Arguments aGreaterNumberOfExpectedItemsMixedProduct() {
        List<Item> expectedItems = asList(aPintOfMilk(), aPintOfKefir(), aPintOfMilk());
        List<Item> items = new ArrayList<>(expectedItems);
        items.add(aPintOfMilk());
        return Arguments.of("a greater number of expected items of mixed product is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, expectedItems, items);
    }

    private static Arguments aExactNumberOfUnexpectedItems() {
        List<Item> items = asList(aPintOfBeer(), aPintOfBeer(), aPintOfBeer());
        return Arguments.of("a exact number of unexpected items is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), items);
    }

    private static Arguments aExactNumberOfExpectedMixedWithUnexpectedItems() {
        Item milk1 = aPintOfMilk();
        Item milk2 = aPintOfMilk();
        Item kefir = aPintOfKefir();
        List<Item> items = asList(milk1, aPintOfBeer(), aPintOfBeer(), milk2, kefir, aPintOfBeer());
        List<Item> expected = asList(milk1, milk2, kefir);
        return Arguments.of("a exact number of expected items mixed with unexpected items is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, expected, items);
    }

}