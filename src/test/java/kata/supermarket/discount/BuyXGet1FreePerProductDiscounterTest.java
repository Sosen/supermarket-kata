package kata.supermarket.discount;

import kata.supermarket.Item;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static kata.supermarket.TestCommons.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BuyXGet1FreePerProductDiscounterTest {

    private static final String REQUIRED_PRODUCT_NAME = "Milk";
    private static final int REQUIRED_ITEMS_COUNT = 2;
    private static final String EXPECTED_DISCOUNT_DESCRIPTION = "Buy 2 of product Milk get 1 free";
    private static final BigDecimal EXPECTED_DISCOUNT = BigDecimal.valueOf(0.49);

    private BuyXGet1FreePerProductDiscounter discounter = new BuyXGet1FreePerProductDiscounter(REQUIRED_PRODUCT_NAME, REQUIRED_ITEMS_COUNT);

    @DisplayName("Discounter applies discount when items are...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void appliedDiscount(String description, BigDecimal expectedDiscount, String expectedDescription, Collection<Item> discountedItems, List<Item> items) {
        DiscountResult result = discounter.applyDiscount(items);
        assertAmount(expectedDiscount, result.getAmount());
        assertEquals(expectedDescription, result.getDescription());
        assertEquals(discountedItems, result.getDiscountedItems());
    }

    @Test
    void testPriority(){
        assertEquals(Discounter.HIGHETS_PRIORITY, discounter.priority());
    }

    static Stream<Arguments> appliedDiscount() {
        return Stream.of(
                aSingleExpectedItem(),
                aLesserNumberOfExpectedItems(),
                aExactNumberOfExpectedItems(),
                aGreaterNumberOfExpectedItems(),
                aExactNumberOfUnexpectedItems(),
                aExactNumberOfExpectedMixedWithUnexpectedItems()
        );
    }

    private static Arguments aSingleExpectedItem() {
        return Arguments.of("a single expected item is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), singletonList(aPintOfMilk()));
    }

    private static Arguments aLesserNumberOfExpectedItems() {
        return Arguments.of("a lesser number of expected items is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), asList(aPintOfMilk(), aPintOfMilk()));
    }

    private static Arguments aExactNumberOfExpectedItems() {
        List<Item> items = asList(aPintOfMilk(), aPintOfMilk(), aPintOfMilk());
        return Arguments.of("a exact number of expected items is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, items, items);
    }

    private static Arguments aGreaterNumberOfExpectedItems() {
        List<Item> expectedItems = asList(aPintOfMilk(), aPintOfMilk(), aPintOfMilk());
        List<Item> items = new ArrayList<>(expectedItems);
        items.add(aPintOfMilk());
        return Arguments.of("a greater number of expected items is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, expectedItems, items);
    }

    private static Arguments aExactNumberOfUnexpectedItems() {
        List<Item> items = asList(aPintOfBeer(), aPintOfBeer(), aPintOfBeer());
        return Arguments.of("a exact number of unexpected items is not discounted", BigDecimal.ZERO, EXPECTED_NO_DISCOUNT_DESCRIPTION, emptyList(), items);
    }

    private static Arguments aExactNumberOfExpectedMixedWithUnexpectedItems() {
        Item milk1 = aPintOfMilk();
        Item milk2 = aPintOfMilk();
        Item milk3 = aPintOfMilk();
        List<Item> items = asList(milk1, aPintOfBeer(), aPintOfBeer(), milk2, milk3, aPintOfBeer());
        List<Item> expected = asList(milk1, milk2, milk3);
        return Arguments.of("a exact number of expected items is discounted", EXPECTED_DISCOUNT, EXPECTED_DISCOUNT_DESCRIPTION, expected, items);
    }
}