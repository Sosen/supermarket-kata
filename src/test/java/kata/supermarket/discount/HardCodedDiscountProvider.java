package kata.supermarket.discount;

import kata.supermarket.ProductCategory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class HardCodedDiscountProvider implements DiscountProvider {
    @Override
    public List<Discounter> provide() {
        return Arrays.asList(
                new BuyXGet1FreePerProductDiscounter("Milk", 1),
                new BuyXForFixedPricePerProductDiscounter("Snickers", 2, BigDecimal.ONE),
                //Next discounter never impact total cause his priority is lower than one above which will be applied first
                new BuyXForFixedPricePerProductDiscounter("Snickers", 2, BigDecimal.valueOf(0.5), Discounter.LOWEST_PRIORITY),
                new BuyXForPriceOfYPerProductDiscounter("Digestive", 3, 2),
                new BuyXOfWeightWithDiscountOfYPercentPerCategoryDiscounter(ProductCategory.SWEETS, BigDecimal.ONE, 50)
        );
    }
}
