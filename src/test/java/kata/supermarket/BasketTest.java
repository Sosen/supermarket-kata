package kata.supermarket;

import kata.supermarket.calculator.DiscountedCalculator;
import kata.supermarket.calculator.TotalCalculator;
import kata.supermarket.discount.DiscountChain;
import kata.supermarket.discount.DiscountProvider;
import kata.supermarket.discount.HardCodedDiscountProvider;
import kata.supermarket.discount.PrioritisedDiscountChain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BasketTest {

    static {
        ServiceLocator.registerInstance(DiscountProvider.class, HardCodedDiscountProvider::new);
        ServiceLocator.registerInstance(DiscountChain.class, PrioritisedDiscountChain::new);
        ServiceLocator.registerInstance(TotalCalculator.class, DiscountedCalculator::new);
    }

    @DisplayName("basket provides its total value when containing...")
    @MethodSource
    @ParameterizedTest(name = "{0}")
    void basketProvidesTotalValue(String description, String expectedTotal, Iterable<Item> items) {
        final Basket basket = new Basket();
        items.forEach(basket::add);
        assertEquals(new BigDecimal(expectedTotal), basket.total());
    }

    static Stream<Arguments> basketProvidesTotalValue() {
        return Stream.of(
                //Discounts not engaged
                noItems(),
                aSingleItemPricedPerUnit(),
                multipleItemsPricedPerUnit(),
                aSingleItemPricedByWeight(),
                multipleItemsPricedByWeight(),
                //Discounts engaged
                aTwoMilkItems(),
                aTwoSnickersItems(),
                aFourSnickersItems(),//test that discount of the same type can be applied more than 1 if remaining items matches give criteria
                aThreeDigestiveItems(),
                aTwoKilosOfSweets(),
                aFourSnickersAndTwoMilkItems()//test that more than 1 discount type can be applied
        );
    }

    private static Arguments aSingleItemPricedByWeight() {
        return Arguments.of("a single weighed item", "1.25", singleton(twoFiftyGramsOfAmericanSweets()));
    }

    private static Arguments multipleItemsPricedByWeight() {
        return Arguments.of("multiple weighed items", "1.85",
                asList(twoFiftyGramsOfAmericanSweets(), twoHundredGramsOfPickAndMix())
        );
    }

    private static Arguments multipleItemsPricedPerUnit() {
        return Arguments.of("multiple items priced per unit", "2.04",
                asList(aPackOfDigestives(), aPintOfMilk()));
    }

    private static Arguments aSingleItemPricedPerUnit() {
        return Arguments.of("a single item priced per unit", "0.49", singleton(aPintOfMilk()));
    }

    private static Arguments aTwoMilkItems() {
        return Arguments.of("a two milk items", "0.49", singleton(aPintOfMilk()));
    }

    private static Arguments aTwoSnickersItems() {
        return Arguments.of("a two Snickers items", "1.00", asList(aSinckersBar(), aSinckersBar()));
    }

    private static Arguments aFourSnickersItems() {
        return Arguments.of("a four Snickers items", "2.00", asList(aSinckersBar(), aSinckersBar(), aSinckersBar(), aSinckersBar()));
    }

    private static Arguments aFourSnickersAndTwoMilkItems() {
        return Arguments.of("a four Snickers and two milk items", "2.49", asList(aSinckersBar(), aSinckersBar(), aPintOfMilk(), aPintOfMilk(), aSinckersBar(), aSinckersBar()));
    }

    private static Arguments aThreeDigestiveItems() {
        return Arguments.of("a three digestive items", "3.10", asList(aPackOfDigestives(), aPackOfDigestives(), aPackOfDigestives()));
    }

    private static Arguments aTwoKilosOfSweets() {
        return Arguments.of("a two kilos of sweets", "3.99", asList(aKiloOfAmericanSweets().weighing(BigDecimal.ONE), aKiloOfPickAndMix().weighing(BigDecimal.ONE)));
    }

    private static Arguments noItems() {
        return Arguments.of("no items", "0.00", Collections.emptyList());
    }

    private static Item aPintOfMilk() {
        return new UnitProduct("Milk", ProductCategory.DAIRY, new BigDecimal("0.49")).oneOf();
    }

    private static Item aPackOfDigestives() {
        return new UnitProduct("Digestive", ProductCategory.SWEETS, new BigDecimal("1.55")).oneOf();
    }

    private static Item aSinckersBar() {
        return new UnitProduct("Snickers", ProductCategory.SWEETS, new BigDecimal("0.6")).oneOf();
    }

    private static WeighedProduct aKiloOfAmericanSweets() {
        return new WeighedProduct("American Sweets", ProductCategory.SWEETS, new BigDecimal("4.99"));
    }

    private static Item twoFiftyGramsOfAmericanSweets() {
        return aKiloOfAmericanSweets().weighing(new BigDecimal(".25"));
    }

    private static WeighedProduct aKiloOfPickAndMix() {
        return new WeighedProduct("Mix", ProductCategory.SWEETS, new BigDecimal("2.99"));
    }

    private static Item twoHundredGramsOfPickAndMix() {
        return aKiloOfPickAndMix().weighing(new BigDecimal(".2"));
    }
}